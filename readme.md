# Score-Cronjob

This is a demo for deploying a cronjob with Score. 

Cronjobs must be deployed through an override file which does not support dynamic configuration out-of-the-box. 
This demo uses [chevron](https://github.com/noahmorrison/chevron) (an implementation of [mustache](http://mustache.github.io/)) in order to render the override file with values retrieved from environment variables.

## Scenario

In this scenario, we pretend that we have an important workload that we want to run as "green" as possible.
With the help of [public API by carbon-aware-computing](https://forecast.carbon-aware-computing.com/swagger/UI), we retrieve the optimal execution time depending on the location of our cloud offering (in this case: germanywestcentral).

We then parse the timestamp from the answer and render it in to the cronjob-override file for humanitec. 

In the end we deploy everything with Score.