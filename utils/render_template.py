import chevron
import os

def render_template(input_file_path: str, output_file_path:str, fallback_value:str):
    with open(input_file_path,'r') as humanitec_override_template_file:
        humanitec_override_template = humanitec_override_template_file.read()
        
    tokens=list(chevron.tokenizer.tokenize(humanitec_override_template))
    variables = [ v[1] for v in tokens if v[0]=='variable']

    variable_mapping={v:os.environ.get(v,fallback_value) for v in variables}

    humanitec_override = chevron.render(humanitec_override_template, variable_mapping)

    with open(output_file_path,'w') as humanitec_override_file:
        humanitec_override_file.write(humanitec_override)

import argparse
parser = argparse.ArgumentParser(
                    prog='Mustache Template Renderer',
                    description='Renders a mustache template with environment variables')
parser.add_argument('-i', '--inputfile', type=str, help='mustache template file', required=True)
parser.add_argument('-o','--outputfile', type=str, default='humanitec.score.yaml', help="File rendered output will be written to. DEFAULT: humanitec.score.yaml")
parser.add_argument('-d','--default-value', type=str, default='11', help="Default value for environment variables that do not exist. DEFAULT: 11")


args=parser.parse_args()

render_template(args.inputfile,args.outputfile, args.default_value)